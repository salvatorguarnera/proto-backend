const { admin, db } = require('../util/admin');
const { v4: uuidv4 } = require('uuid');
const {validateClassData, validateAddStudentsToClass} = require('../util/validators');

exports.createClass = (req, res) => {

    const classID = uuidv4();
    const teacherID = req.body.teacherID;
    const classCode = req.body.classCode
    const className = req.body.className;
    const studentList = req.body.studentList;

    const classObj = {
        classCode : classCode,
        teacherID : teacherID,
        className : className,
        studentList : studentList,
        classID : classID
    };

    const { valid, errors } = validateClassData(classObj);
    if(!valid) return res.status(400).json(errors);

    db
        .doc(`/classes/${classID}/`)
        .set(classObj)
        .then(() => {

            return db
                    .doc(`/teachers/${teacherID}/`)
                    .update({
                        classList: admin.firestore.FieldValue.arrayUnion(classID)
                    });

        })
        .then(() => {

            return res.status(200).json({message: `Successfully created class ${classID} for teacher ${teacherID}`});

        })
        .catch((err) => {

            console.error(err);
            return res.status(500).json({error : err.code});

        });

}

exports.addStudentsToClass = (req,res) => { 

    const { valid, errors } = validateAddStudentsToClass(req.body);
    if(!valid) return res.status(400).json(errors);

    let incomingStudentList = req.body.studentList;
    const classID = req.body.classID;

    admin.firestore().doc('classes/'.concat(classID)).get()
        .then(snapshot =>{
            const data = snapshot.data();

            if (!data){
                //This should actually cause the function to bomb because the class doesn't exist.
                return res.status(404).json({message: `Class ID : ${classID} does not exist`});
            }

            let studentList = data.studentList;
            incomingStudentList.forEach(element => {
                if (studentList.includes(element) !== true){
                    studentList.push(element);
                }
            });
            const dat = {
                studentList:studentList
            };

            return db
                .collection('classes')
                .doc(classID)
                .update(dat)
                .then(() => {

                    let promiseArray = [];

                    incomingStudentList.forEach(studentID => {

                        const singlePromise = admin.firestore().doc(`/students/${studentID}/`).update({
                                classList: admin.firestore.FieldValue.arrayUnion(classID)
                            });
                        promiseArray.push(singlePromise);
                    })

                    return Promise.all(promiseArray);

                });
                
        })
            .then(()=>{
                return res.status(200).json({message:"Sucessfully added Students to class"});
        })
            .catch(error =>{
                console.error(error);
                return res.status(500).json({message:error});
            });
    
};

exports.getClass = async (req, res) => {

    let classData = {};
    const userID = req.body.userID;
    const isTeacher = req.body.isTeacher;

    let collection = (isTeacher) ? "teachers" : "students";

    db
        .doc(`${collection}/${userID}/`)
        .get()
        .then((doc) => {

            return doc.data().classList;

        })
        .then((classList) => {

            let promiseArray = [];
            classList.forEach(function(classID){

                const classInfo = db.doc(`classes/${classID}/`).get();
                promiseArray.push(classInfo);

            })

            return Promise.all(promiseArray);
        })
        .then((classSnapshots) => {

            classSnapshots.forEach(function(snapshot, index){

                console.log(snapshot.data());
                classData[index] = snapshot.data();

            })

            return res.status(200).json(classData);

        })
        .catch((err) => {
            console.error(err);
            return res.status(500).json({message : err.code});
        })


}

exports.createAssignment = (req, res) => {

    const classID = req.body.classID;

    let assignmentObj = {
        creationTime : admin.firestore.FieldValue.serverTimestamp(),
        dueDateTime : req.body.dueDateTime,
        homeworkName : req.body.homeworkName,
        description : req.body.description,
        classID : classID,
        teacherID : req.body.teacherID,
        fileLink : req.body.fileLink
    }

    db
        .collection('classes')
        .doc(classID)
        .collection('assignments')
        .add(assignmentObj)
        .then((doc) => {
            return res.status(200).json({message: `Assignment: ${doc.id} created for class: ${classID} `});
        })
        .catch((err) => {
            console.error(err);
            return res.status(500).json({error : err.code});
        });

}

exports.getAssignments = (req, res) => {

    const classID = req.params.classID;

    db
        .collection("classes")
        .doc(classID)
        .collection("assignments")
        .get()
        .then((snapshots) => {

            let assignmentArray = [];
            if(snapshots.docs.length > 0){

                snapshots.forEach(function(snap){

                    let obj = snap.data()
                    assignmentArray.push(obj);

                })

            }

            return res.status(200).json(assignmentArray);

        })
        .catch((err) => {
            console.error(err);
            return res.status(500).json({message : err});
        })

}

exports.getClassList = (req, res) => {

    const classID = req.params.classID;
    var teacherName;
    var studentList = [] ;
    const classNameArray = [];

    db.collection('classes')
        .doc(classID)
        .get()
        .then((snapshot) => {

            const classData = snapshot.data();
            classData.studentList.forEach(student => {
                studentList.push(student);
            })

            return classData.teacherID;

        })
        .then((teacherID) => {

            return db.collection('teachers').doc(teacherID).get();

        })
        .then((teacherSnapshot) => {

            teacherName = teacherSnapshot.data().name;
            return studentList;

        })
        .then(() => {

            let promiseArray = [];
            studentList.forEach((studentID) => {
                console.log(studentID);
                let promise = db.collection('students').doc(studentID).get();
                promiseArray.push(promise);
            })

            return Promise.all(promiseArray);

        })
        .then((studentSnapshots) => {

            classNameArray.push(teacherName);
            studentSnapshots.forEach(function(studentSnap, index){

                let name = studentSnap.data().name;
                classNameArray.push(name);

            });

            return res.status(200).json(classNameArray);

        })
        .catch((err) => {
            console.error(err);
            return res.status(500).json({error : err.code});
        })

}

