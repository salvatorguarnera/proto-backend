const { admin, db } = require('../util/admin');
const { v4: uuidv4 } = require('uuid');

exports.createMessage = (req, res) => {


    const existingChannel = req.body.existingChannel;
    
    if(existingChannel){

        const channelID = req.body.channelID;
        let messageObj = {

            body: req.body.body,
            toID: req.body.toID,
            fromID: req.body.fromID,
            timestamp: admin.firestore.FieldValue.serverTimestamp(),
            read: false

        }

        db
            .collection('channels')
            .doc(channelID)
            .collection('messages')
            .add(messageObj)
            .then((doc) => {

                return res.status(200).json({message: `Successfully created new message : ${doc.id} under channel: ${channelID}`});

            })
            .catch((err) => {
                console.error(err);
                return res.status(500).json({error: `Could not create new message under channel: ${channelID}`});
            })

    }else{

        const toID = req.body.toID;
        const fromID = req.body.fromID;
        const newChannelID = uuidv4();

        const toDirectory = (req.body.toIsTeacher) ? "teachers" : "students";
        const fromDirectory = (req.body.fromIsTeacher) ? "teachers" : "students";

        db.collection(toDirectory).doc(toID).update({
            messageChannels: admin.firestore.FieldValue.arrayUnion(newChannelID)
        })
        .then(() => {

            return db.collection(fromDirectory).doc(fromID).update({
                messageChannels: admin.firestore.FieldValue.arrayUnion(newChannelID)
            });

        })
        .then(() => {

            let messageObj = {

                body: req.body.body,
                toID: toID,
                fromID: fromID,
                timestamp: admin.firestore.FieldValue.serverTimestamp(),
                read: false
    
            };

            return db
                    .collection('channels')
                    .doc(newChannelID)
                    .collection('messages')
                    .add(messageObj);

        })
        .then((doc) => {
            return res.status(200).json({message: `Successfully created channel: ${newChannelID} and message: ${doc.id}`})
        })
        .catch((err) => {

            console.error(err);
            return res.status(500).json({error : "Issue creating new messages channel..."});

        })

    }


}

exports.getConversation = (req, res) => {

    const channelID = req.params.channelID;
    
    db
        .collection('channels')
        .doc(channelID)
        .collection('messages')
        .get()
        .then((messageSnapshots) => {
            let messageArr = [];

            messageSnapshots.forEach(function(snapshot, index){
                messageArr.push(snapshot.data());
            });

            return res.status(200).json(messageArr);
        })
        .catch((err) => {
            console.error(err);
            return res.status(500).json({error: err.code});
        })

}

exports.getChannels = (req, res) => {

    const userID = req.params.userID;

    db
        .collection("students")
        .doc(userID)
        .get()
        .then((doc) => {

            if(doc.exists){

                let userData = doc.data();
                let channelArray = (userData.messageChannels) ? (userData.messageChannels) : [];

                return channelArray;

            }else{

                return [];

            }

        })
        .then((potentialArray) => {

            if(potentialArray.length === 0){

                db.collection("teachers").doc(userID).get().then((doc) => {
                    let userData = doc.data();
                    let channelArray = (userData.messageChannels) ? (userData.messageChannels) : [];

                    return res.status(200).json(channelArray);
                })

            }else{
                return res.status(200).json(potentialArray);
            }

        })
        .catch((err) => {
            console.error(err);
            return res.status(500).json({message : err.code});
        })

}