const { admin, db } = require('../util/admin');
const {validateLoginData, validateSignUpData} = require('../util/validators');

exports.login = (req, res) => {

    const user = {
        email: req.body.email,
        password: req.body.password
    };

    const { valid, errors } = validateLoginData(user);

    if(!valid) return res.status(400).json(errors);

    admin
        .auth()
        .signInWithEmailAndPassword(user.email, user.password)
        .then((data) => {
            return data.user.getIdToken();
        })
        .then((token) => {
            return res.json({token});
        })
        .catch((err) => {
            console.error(err);
            return res.status(403).json({general: 'Wrong credentials, please try again'});
        });
};


exports.signup = (req, res) => {
    
    const userinfo ={
        email : req.body.email,
        password : req.body.password,
        name : req.body.name,
        isTeacher : req.body.isTeacher,
        idNum : req.body.idNum,
        classList : req.body.classList
    }

    const {valid,errors} = validateSignUpData(userinfo);
    if (!valid) return res.status(400).json(errors);
    let prelimDataObj;
    if (userinfo.isTeacher){
        prelimDataObj = {
            email : userinfo.email,
            name : userinfo.name,
            isTeacher : userinfo.isTeacher
        };
    }
    else{
        if (! userinfo.classList) userinfo.classList = [];
        prelimDataObj = {
            email : userinfo.email,
            name : userinfo.name,
            isTeacher : userinfo.isTeacher,
            classList: userinfo.classList
        };
    };

    admin.auth().createUser({email: userinfo.email, password: userinfo.password})
        .then((user) => {

            const uid = user.uid;

            if(userinfo.isTeacher){

                const newDataObj = {
                    ...prelimDataObj,
                    teacherID: uid
                };

                return db.doc(`/teachers/${uid}/`)
                    .set(newDataObj);
            }else{

                const newDataObj = {
                    ...prelimDataObj,
                    studentID: uid
                };

                return db.doc(`/students/${uid}/`)
                    .set(newDataObj);
            }

        })
        .then(() => {
            return res.status(200).json({message : "Successfully created user and added to database."});
        })
        .catch((err) => {
            console.error(err);
            return res.status(500).json({error: err.code});
        })

};

exports.getUser = (req, res) => {

    let userData = {};
    db.doc(`/students/${req.params.userID}`)
        .get()
        .then((doc) => {

            if(!doc.exists){
                return {};
            }else{

                userData = doc.data();
                userData.uid = doc.id;
                return userData;

            }


        })
        .then((updatedUserDoc) => {

            const isTeacher = Object.keys(updatedUserDoc).length === 0 ? true : false;
            if(isTeacher){

                return db.doc(`/teachers/${req.params.userID}`)
                    .get()
                    .then((doc) => {

                        if(!doc.exists){
                            return res.status(404).json({message : "User not found..."});
                        }else{

                            updatedUserDoc = doc.data();
                            updatedUserDoc.uid = doc.id;
                            return res.status(200).json(updatedUserDoc);
                        }

                    });

            }else{
                return res.status(200).json(updatedUserDoc);
            }
        })
        .catch((err) => {
            console.error(err);
            return res.status(500).json({error: err.code});
        });
};