const functions = require('firebase-functions');
const app = require('express')()
const cors = require('cors');
app.use(cors({origin: true}));
// Create and Deploy Your First Cloud Functions
// https://firebase.google.com/docs/functions/write-firebase-functions

const {login, signup, getUser} = require('./handlers/users');
const {createClass, addStudentsToClass, getClass, createAssignment, getClassList, getAssignments} = require('./handlers/classes');
const {createMessage, getConversation, getChannels} = require('./handlers/messages');

const fbAuth = require('./util/fbAuth');

app.post('/login', login);
app.post('/signup', signup);
app.post('/createClass', createClass);
app.post('/getClass', getClass);
app.post('/addStudentsToClass', addStudentsToClass);
app.post('/createMessage', createMessage);
app.post('/createAssignment', createAssignment);
//when using postman to call this function, your request http should be...
//{{dev}}/getStudent/c4Fqt60KRzXYLVSoJoYi
//Where 'c4Fqt60KRzXYLVSoJoYi' is the studentID
//This is an example of a uuid in the pathway.
app.get('/getUser/:userID', getUser);
app.get('/getConversation/:channelID', getConversation);
app.get('/getClassList/:classID', getClassList);
app.get('/getChannels/:userID', getChannels);
app.get('/getAssignments/:classID', getAssignments);


exports.api = functions.https.onRequest(app);
