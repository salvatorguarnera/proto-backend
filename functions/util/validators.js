const { admin, db } = require('../util/admin');

const isEmpty = (string) => {
    if (string.trim() === '') return true;
    else return false;
};

exports.validateLoginData = (data) => {
    let errors = {};
  
    if (isEmpty(data.email)) errors.email = 'Must not be empty';
    if (isEmpty(data.password)) errors.password = 'Must not be empty';
  
    return {
      errors,
      valid: Object.keys(errors).length === 0 ? true : false
    };
};

exports.validateSignUpData = (data) => {
  let errors = {};

  if (isEmpty(data.email)) errors.email = 'Must not be empty';
  if (isEmpty(data.password)) errors.password = 'Must not be empty';
  if (isEmpty(data.name)) errors.name = 'Must not be empty';
  if (!data.isTeacher){
    if (data.idNum < 0) errors.idNum = 'Must be a Positive integer';
  }
  return {
    errors,
    valid: Object.keys(errors).length === 0 ? true : false
  };
  
}

exports.validateClassData = (data) => {

  let errors = {};

  if(isEmpty(data.teacherID)) errors.teacherID = 'Must not be empty';
  if(isEmpty(data.classCode)) errors.classCode = 'Must not be empty';
  if(isEmpty(data.className)) errors.className = 'Must not be empty';
  
  return {
    errors,
    valid : Object.keys(errors).length === 0 ? true : false
  };

}

exports.validateAddStudentsToClass = (data)=>{
  let errors = {};
  if(!data.studentList) errors.studentList = 'Must not be empty';
  if (data.studentList.length === 0) errors.studentList = 'Must not be empty';

  if(isEmpty(data.classID)) errors.classID = 'Must not be empty';
  return {
    errors,
    valid : Object.keys(errors).length === 0 ? true : false
  };
}
